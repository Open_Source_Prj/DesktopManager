﻿extern alias SP;
using SP::Manager.SettingsParser;
using System;
using System.IO;
using System.Linq;

namespace Manager.Core
{
    class DesktopManager
    {
        private readonly string _desktopPath;
        private readonly string _moveFolderPath;
        private string[] _desktopFiles;
        private SettingsParser xml = new SettingsParser();
        private Logger _log;

        public DesktopManager()
        {
            //_log = new Logger();
            //_desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            //if (!xml.Load())
            //{
            //    string msg = string.Format("{0} was not found! Please generate {0} first!", xml.SettingsXmlName);
            //    _log.PrintError(msg);
            //}
            //_moveFolderPath = _desktopPath + "\\" + xml.SettingsXml.MoveTo.FolderName;
            //_desktopFiles = ReadDesktopFiles();
            //CreateDirectory(xml.SettingsXml.MoveTo.FolderName);
        }

        public void Clean()
        {
            //foreach (var file in _desktopFiles)
            //{
            //    if (xml.SettingsXml.MoveExtension.FirstOrDefault(x => x == Path.GetExtension(file)) != null)
            //    {
            //        MoveFile(file, _moveFolderPath);
            //    }
            //}
            //_log.PrintStatus();
        }

        private void CreateDirectory(string name)
        {
            string directory = _desktopPath + "\\" + name;
            if (!Directory.Exists(directory))
            {
                DirectoryInfo dir = Directory.CreateDirectory(directory);
                string msg = string.Format("Directory \"{0}\" was created successfully on {1}.", name, _desktopPath);
                _log.PrintMessage(msg, false);
            }
        }

        private string[] ReadDesktopFiles()
        {
            return Directory.GetFiles(_desktopPath);
        }

        private void MoveFile(string sourceFileName, string destFolder)
        {
            string sourceName = Path.GetFileName(sourceFileName);
            string destFileName = destFolder + "\\" + sourceName;
            if (!File.Exists(destFileName))
            {
                File.Move(sourceFileName, destFileName);
                string msg = string.Format("{0} was moved to {1}.", sourceName, destFolder);
                _log.PrintMessage(msg);
            }
            else
            {
                File.Delete(sourceFileName);
                string msg = string.Format("{0} deleted from {1}. Already exists in {2}", sourceName, _desktopPath, destFolder);
                _log.PrintMessage(msg);
            }
        }
    }
}
