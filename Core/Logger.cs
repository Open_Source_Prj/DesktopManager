﻿using System;
using System.IO;

namespace Manager.Core
{
    class Logger
    {
        private readonly DateTime _date = DateTime.Now;
        private const string _logFileName = "DesktopManager.log";
        private readonly string _logFilePath;
        private string _time;
        private uint _ct = 0;

        public Logger()
        {
            _logFilePath = AppDomain.CurrentDomain.BaseDirectory + _logFileName;
            _time = _date.ToString("dd/MMM/yyyy HH:mm:ss");
            InitializeLogger();
        }

        public void PrintMessage(string message, bool increment = true)
        {
            string msg;
            if(increment)
            {
                _ct++;
                msg = string.Format("{0}: {1}", _ct, message);
            }
            else
            {
                msg = message;
            }

            Console.WriteLine(msg);
            WriteToLog(msg);
        }

        public void PrintError(string message)
        {
            string msg = string.Format("Error: {0}", message);
            Console.WriteLine(msg);
            WriteToLog(msg);
            WriteToLog("-----------------------------------------------------------------------------");
            Console.WriteLine("Press any key to continue . . .");
            Console.ReadKey();
            Environment.Exit(0);
        }

        public void PrintStatus()
        {
            string msg = string.Format("{0} files moved.", _ct);
            Console.WriteLine(msg);
            WriteToLog(msg);
            WriteToLog("-----------------------------------------------------------------------------");
            Console.WriteLine("All of the above information was written in the log file {0}.", _logFilePath);
            Console.WriteLine("DesktopManager finished successfully. Press any key to continue . . .");
            Console.ReadKey();
        }

        private void InitializeLogger()
        {
            if (!File.Exists(_logFilePath))
            {
                File.Create(_logFilePath).Close();
                TextWriter tw = new StreamWriter(_logFilePath);
                tw.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~DesktopManager Logger~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                tw.WriteLine();
                tw.Close();
            }

            TextWriter tw2 = new StreamWriter(_logFilePath, true);
            tw2.WriteLine(_time);
            tw2.WriteLine();
            tw2.Close();
        }

        private void WriteToLog(string message)
        {
            TextWriter tw = new StreamWriter(_logFilePath, true);
            tw.WriteLine(message);
            tw.Close();
        }
    }
}
