﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manager.Core
{
    class Program
    {
        private static DesktopManager manager;

        static void Main(string[] args)
        {
            manager = new DesktopManager();
            manager.Clean();
        }
    }
}
