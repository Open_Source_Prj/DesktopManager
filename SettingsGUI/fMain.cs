﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Manager.GUI
{
    public partial class fMain : Form, IFormMain
    {
        private FormMainPresenter _presenter;
        public fMain()
        {
            InitializeComponent();
            _presenter = new FormMainPresenter(this);
        }

        public string TargetFolder
        {
            get
            {
                return textBoxTargetFolder.Text;
            }
        }

        public string MoveTo
        {
            get
            {
                return textBoxMoveTo.Text;
            }
        }

        public string Extension
        {
            get
            {
                return textBoxExtension.Text;
            }
        }

        string IFormMain.Contains
        {
            get
            {
                return textBoxContains.Text;
            }
        } 

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void fMain_Load(object sender, EventArgs e)
        {

        }

        private void buttonAddSettings_Click(object sender, EventArgs e)
        {
            _presenter.AddToSettings();
        }
    }
}
