﻿namespace Manager.GUI
{
    partial class fMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExit = new System.Windows.Forms.Button();
            this.textBoxTargetFolder = new System.Windows.Forms.TextBox();
            this.labelTargetFolder = new System.Windows.Forms.Label();
            this.labelMoveTo = new System.Windows.Forms.Label();
            this.labelExtension = new System.Windows.Forms.Label();
            this.textBoxMoveTo = new System.Windows.Forms.TextBox();
            this.comboBoxMoveToCreationMode = new System.Windows.Forms.ComboBox();
            this.comboBoxCopyMode = new System.Windows.Forms.ComboBox();
            this.labelContains = new System.Windows.Forms.Label();
            this.textBoxExtension = new System.Windows.Forms.TextBox();
            this.textBoxContains = new System.Windows.Forms.TextBox();
            this.buttonAddSettings = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(880, 588);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 0;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // textBoxTargetFolder
            // 
            this.textBoxTargetFolder.Location = new System.Drawing.Point(126, 59);
            this.textBoxTargetFolder.Name = "textBoxTargetFolder";
            this.textBoxTargetFolder.Size = new System.Drawing.Size(224, 22);
            this.textBoxTargetFolder.TabIndex = 1;
            // 
            // labelTargetFolder
            // 
            this.labelTargetFolder.AutoSize = true;
            this.labelTargetFolder.Location = new System.Drawing.Point(30, 59);
            this.labelTargetFolder.Name = "labelTargetFolder";
            this.labelTargetFolder.Size = new System.Drawing.Size(90, 17);
            this.labelTargetFolder.TabIndex = 2;
            this.labelTargetFolder.Text = "TargetFolder";
            // 
            // labelMoveTo
            // 
            this.labelMoveTo.AutoSize = true;
            this.labelMoveTo.Location = new System.Drawing.Point(30, 99);
            this.labelMoveTo.Name = "labelMoveTo";
            this.labelMoveTo.Size = new System.Drawing.Size(63, 17);
            this.labelMoveTo.TabIndex = 3;
            this.labelMoveTo.Text = "Move To";
            // 
            // labelExtension
            // 
            this.labelExtension.AutoSize = true;
            this.labelExtension.Location = new System.Drawing.Point(30, 144);
            this.labelExtension.Name = "labelExtension";
            this.labelExtension.Size = new System.Drawing.Size(69, 17);
            this.labelExtension.TabIndex = 4;
            this.labelExtension.Text = "Extension";
            // 
            // textBoxMoveTo
            // 
            this.textBoxMoveTo.Location = new System.Drawing.Point(126, 99);
            this.textBoxMoveTo.Name = "textBoxMoveTo";
            this.textBoxMoveTo.Size = new System.Drawing.Size(224, 22);
            this.textBoxMoveTo.TabIndex = 5;
            // 
            // comboBoxMoveToCreationMode
            // 
            this.comboBoxMoveToCreationMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMoveToCreationMode.FormattingEnabled = true;
            this.comboBoxMoveToCreationMode.Items.AddRange(new object[] {
            "Create",
            "Alert"});
            this.comboBoxMoveToCreationMode.Location = new System.Drawing.Point(829, 69);
            this.comboBoxMoveToCreationMode.Name = "comboBoxMoveToCreationMode";
            this.comboBoxMoveToCreationMode.Size = new System.Drawing.Size(111, 24);
            this.comboBoxMoveToCreationMode.TabIndex = 6;
            // 
            // comboBoxCopyMode
            // 
            this.comboBoxCopyMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCopyMode.FormattingEnabled = true;
            this.comboBoxCopyMode.Items.AddRange(new object[] {
            "Rename",
            "Override",
            "Alert"});
            this.comboBoxCopyMode.Location = new System.Drawing.Point(829, 99);
            this.comboBoxCopyMode.Name = "comboBoxCopyMode";
            this.comboBoxCopyMode.Size = new System.Drawing.Size(111, 24);
            this.comboBoxCopyMode.TabIndex = 7;
            // 
            // labelContains
            // 
            this.labelContains.AutoSize = true;
            this.labelContains.Location = new System.Drawing.Point(30, 181);
            this.labelContains.Name = "labelContains";
            this.labelContains.Size = new System.Drawing.Size(63, 17);
            this.labelContains.TabIndex = 8;
            this.labelContains.Text = "Contains";
            // 
            // textBoxExtension
            // 
            this.textBoxExtension.Location = new System.Drawing.Point(126, 144);
            this.textBoxExtension.Name = "textBoxExtension";
            this.textBoxExtension.Size = new System.Drawing.Size(224, 22);
            this.textBoxExtension.TabIndex = 9;
            // 
            // textBoxContains
            // 
            this.textBoxContains.Location = new System.Drawing.Point(126, 181);
            this.textBoxContains.Name = "textBoxContains";
            this.textBoxContains.Size = new System.Drawing.Size(224, 22);
            this.textBoxContains.TabIndex = 10;
            // 
            // buttonAddSettings
            // 
            this.buttonAddSettings.Location = new System.Drawing.Point(33, 245);
            this.buttonAddSettings.Name = "buttonAddSettings";
            this.buttonAddSettings.Size = new System.Drawing.Size(119, 35);
            this.buttonAddSettings.TabIndex = 11;
            this.buttonAddSettings.Text = "Add";
            this.buttonAddSettings.UseVisualStyleBackColor = true;
            this.buttonAddSettings.Click += new System.EventHandler(this.buttonAddSettings_Click);
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(967, 623);
            this.Controls.Add(this.buttonAddSettings);
            this.Controls.Add(this.textBoxContains);
            this.Controls.Add(this.textBoxExtension);
            this.Controls.Add(this.labelContains);
            this.Controls.Add(this.comboBoxCopyMode);
            this.Controls.Add(this.comboBoxMoveToCreationMode);
            this.Controls.Add(this.textBoxMoveTo);
            this.Controls.Add(this.labelExtension);
            this.Controls.Add(this.labelMoveTo);
            this.Controls.Add(this.labelTargetFolder);
            this.Controls.Add(this.textBoxTargetFolder);
            this.Controls.Add(this.btnExit);
            this.Name = "fMain";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DesktopManager Settings";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.fMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.TextBox textBoxTargetFolder;
        private System.Windows.Forms.Label labelTargetFolder;
        private System.Windows.Forms.Label labelMoveTo;
        private System.Windows.Forms.Label labelExtension;
        private System.Windows.Forms.TextBox textBoxMoveTo;
        private System.Windows.Forms.ComboBox comboBoxMoveToCreationMode;
        private System.Windows.Forms.ComboBox comboBoxCopyMode;
        private System.Windows.Forms.Label labelContains;
        private System.Windows.Forms.TextBox textBoxExtension;
        private System.Windows.Forms.TextBox textBoxContains;
        private System.Windows.Forms.Button buttonAddSettings;
    }
}

