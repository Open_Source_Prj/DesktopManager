﻿using Google.Protobuf;
using System;
using System.IO;

namespace Manager.SettingsParser
{
    public class ProtoParser : IParser
    {
        private readonly string _protoFilePath;

        public ProtoParser(string fileName)
        {
            FileName = fileName;
            _protoFilePath = AppDomain.CurrentDomain.BaseDirectory + FileName;
        }

        public string FileName { get; }

        public SettingsProto SettingsProtobuf { get; set; }

        public void Load()
        {
            if(!File.Exists(_protoFilePath))
            {
                throw new FileNotFoundException($"{_protoFilePath} not found.");
            }

            try
            {
                using (var input = File.OpenRead(_protoFilePath))
                {
                    SettingsProtobuf = SettingsProto.Parser.ParseFrom(input);
                }
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        public void Save()
        {
            using (var output = File.Create(_protoFilePath))
            {
                SettingsProtobuf.WriteTo(output);
            }
        }
    }
}
