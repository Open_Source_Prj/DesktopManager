﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manager.SettingsParser
{
    interface IParser
    {
        string FileName { get; }

        bool Load();
        void Save();
    }

    public enum CreationMode
    {
        D_CREATE     = 0,
        D_ALERT      = 1
    }

    public enum CopyMode
    {
        F_RENAME     = 0,
        F_OVERRIDE   = 1,
        F_ALERT      = 2
    }
}