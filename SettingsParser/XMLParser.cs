﻿using Manager.SettingsParser.XML;
using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Manager.SettingsParser
{
    class XMLParser : IParser
    {
        private readonly string _settingsXmlPath;

        public XMLParser(string fileName)
        {
            FileName = fileName;
            SettingsXml = new SettingsXml();
            _settingsXmlPath = AppDomain.CurrentDomain.BaseDirectory + FileName;
        }

        public string FileName { get; }
        public SettingsXml SettingsXml { get; set; }

        public bool Load()
        {
            if(!File.Exists(_settingsXmlPath))
            {
                return false;
                //throw new FileNotFoundException($"{FileName} not found.");
            }

            XmlSerializer serializaer = new XmlSerializer(typeof(SettingsXml));
            FileStream sourceStream = File.Open(_settingsXmlPath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
            SettingsXml = (SettingsXml)serializaer.Deserialize(sourceStream);
            sourceStream.Dispose();

            return true;
        }

        public void Save()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(SettingsXml));
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            XmlWriterSettings xws = new XmlWriterSettings { Indent = true };
            Stream writer = new FileStream(_settingsXmlPath, FileMode.Create);
            XmlWriter xw = XmlWriter.Create(writer, xws);
            serializer.Serialize(xw, SettingsXml, ns);
            writer.Seek(0, SeekOrigin.Begin);
            SettingsXml = (SettingsXml)serializer.Deserialize(writer);
            writer.Close();
        }
        public void GenerateDefaultSettings()
        {
            string moveTo = "Other";
            string[] moveExtension = { ".png", ".jpg" };

            SettingsXml.MoveTo = new SettingsMoveTo(moveTo);
            SettingsXml.MoveExtension = moveExtension;

            Save();
        }
    }
}
